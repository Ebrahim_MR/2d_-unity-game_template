﻿using UnityEngine;
using System.Collections;

public class GUIController : MonoBehaviour
{

    public AudioClip Hit, Score;

    internal void SetPause(bool val)
    {
        transform.GetChild(1).gameObject.SetActive(val);
        transform.GetChild(2).gameObject.SetActive(val);
    }

    internal void SetGameOver(bool val)
    {
        transform.GetChild(1).gameObject.SetActive(val);
        transform.GetChild(3).gameObject.SetActive(val);
    }

    internal void SetWin(bool val)
    {
        transform.GetChild(1).gameObject.SetActive(val);
        transform.GetChild(4).gameObject.SetActive(val);
    }
}
