﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Controller : MonoBehaviour
{
    /// <summary>
    /// CONTROL
    /// </summary>
    private bool paused, gameOver, win;
    public int MAXSCORE, HEALTH;


    /// <summary>
    /// GUI
    /// </summary>
    /// 
    public GameObject player;
    public Text text;
    private GUIController guic;
    private int health, score;

    void Awake()
    {
        if (MAXSCORE == 0)
        {
            Debug.LogWarning("MAXSCORE is not set");
        }
        if (HEALTH == 0)
        {
            Debug.LogWarning("HEALTH is not set");
        }
        if (player == null)
        {
            Debug.LogWarning("Player is set to NULL");
        }
        if (text == null)
        {
            Debug.LogWarning("Text is set to NULL");
        }
        guic = GameObject.FindGameObjectWithTag("GUI").GetComponent<GUIController>();
        if (guic == null)
        {
            Debug.LogWarning("guic is set to NULL");
        }
    }

    // Use this for initialization
    void Start()
    {
        //_maxTarget = (byte)Holder.transform.childCount;

        Time.timeScale = 1.0f;
        paused = false;
        gameOver = false;
        win = false;
        score = 0;
        health = HEALTH;
        updateScore();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            paused = !paused;
            if (paused == false)
                resume();
        }
    }

    private void updateScore()
    {
        text.text = score.ToString() + " / " + MAXSCORE;
        if (score >= MAXSCORE)
        {
            win = true;
        }
        else if (HEALTH <= 0)
        {
            gameOver = true;
        }
    }

    private void OnGUI()
    {
        if (gameOver)
        {
            Time.timeScale = 0.0f;
            guic.SetGameOver(true);
        }

        if (paused)
        {
            Time.timeScale = 0.0f;
            guic.SetPause(true);
        }

        if (win)
        {
            Time.timeScale = 0.0f;
            guic.SetWin(true);
        }
    }

    internal void AddScore()
    {
        score++;
        updateScore();
        AudioSource.PlayClipAtPoint(guic.Score, player.transform.position);
    }

    internal void LoseHealth()
    {
        health--;
        updateScore();
        AudioSource.PlayClipAtPoint(guic.Hit, player.transform.position);
    }

    public void resume()
    {
        Time.timeScale = 1.0f;
        paused = false;
        guic.SetPause(false);
    }

    public void restart()
    {
        Application.LoadLevel(0);
    }

    public void quit()
    {
        Application.Quit();
    }
}